#include <climits>

#include "DDImage/Iop.h"
#include "DDImage/Knobs.h"
#include "DDImage/Row.h"
#include "DDImage/CameraOp.h"
#include "DDImage/LightOp.h"
#include "DDImage/RenderScene.h"

#include "gvdb.h"

static const char* const CLASS = "VoxelRender";
static const char* const HELP = "Renders sparse volumetric data using NVIDIA GVDB on GPU";

using namespace DD::Image;

class VoxelRender : public RenderScene
{
public:
    VoxelRender(::Node* node);

    // Nuke methods
    virtual void knobs(Knob_Callback);
    int knob_changed(Knob* k);

    // void append(Hash& hash);
    void _validate(bool);
    void _open();
    void engine(int y, int x, int r, ChannelMask channels, Row& row);

    const char* Class() const { return CLASS; }
	const char* displayName() const { return "VoxelRender"; }
	const char* node_help() const { return HELP; }
    static const Iop::Description description;

private:
// GVDB stuff
    VolumeGVDB                  gvdb;
    unsigned char*              buf;

// Nuke knobs
};