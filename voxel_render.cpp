#include "voxel_render.h"

using namespace DD::Image;

VoxelRender::VoxelRender(::Node* node) : RenderScene(node)
{
    // initialize values
    gvdb.SetDebug(true);
    gvdb.SetVerbose (true);
    gvdb.SetCudaDevice (GVDB_DEV_FIRST);
	gvdb.Initialize();
}

void VoxelRender::knobs(Knob_Callback f)
{
    RenderScene::knobs(f);
}

int VoxelRender::knob_changed(Knob* k)
{
    return Iop::knob_changed(k);
}

void VoxelRender::_validate(bool for_real)
{

}

void VoxelRender::_open()
{
	int w = 512;
	int h = 512;
	
    // Load VBX
	char scnpath[1024];		
	if (!gvdb.FindFile("explosion.vbx", scnpath)) 
    {
		error( "Cannot find vbx file" );
	}
	printf("Loading VBX. %s\n", scnpath);
	gvdb.LoadVBX (scnpath);

    // Set volume params
	gvdb.getScene()->SetSteps ( .25f, 16, .25f );			// Set raycasting steps
	gvdb.getScene()->SetExtinct ( -1.0f, 1.5f, 0.0f );		// Set volume extinction
	gvdb.getScene()->SetVolumeRange ( 0.1f, 0.0f, .1f );	// Set volume value range
	gvdb.getScene()->SetCutoff ( 0.005f, 0.01f, 0.0f );
	gvdb.getScene()->SetBackgroundClr ( 0.1f, 0.2f, 0.4f, 1.0 );
	gvdb.getScene()->LinearTransferFunc ( 0.00f, 0.25f, Vector4DF(0,0,0,0), Vector4DF(1,1,0,0.1f) );
	gvdb.getScene()->LinearTransferFunc ( 0.25f, 0.50f, Vector4DF(1,1,0,0.4f), Vector4DF(1,0,0,0.3f) );
	gvdb.getScene()->LinearTransferFunc ( 0.50f, 0.75f, Vector4DF(1,0,0,0.3f), Vector4DF(.2f,.2f,0.2f,0.1f) );
	gvdb.getScene()->LinearTransferFunc ( 0.75f, 1.00f, Vector4DF(.2f,.2f,0.2f,0.1f), Vector4DF(0,0,0,0.0) );
	gvdb.CommitTransferFunc ();

    Camera3D* cam = new Camera3D;						// Create Camera 
	cam->setFov ( 50.0 );
	cam->setOrbit ( Vector3DF(20,30,0), Vector3DF(125,160,125), 500, 1.0f );	
	gvdb.getScene()->SetCamera( cam );	
	gvdb.getScene()->SetRes ( w, h );
	
	Light* lgt = new Light;								// Create Light
	lgt->setOrbit ( Vector3DF(299,57.3f,0), Vector3DF(132,-20,50), 200, 1.0f );
	gvdb.getScene()->SetLight ( 0, lgt );		
	
	printf ( "Creating screen buffer. %d x %d\n", w, h );
	gvdb.AddRenderBuf ( 0, w, h, 4 );					// Add render buffer 
    
    gvdb.TimerStart ();
	gvdb.Render ( SHADE_VOLUME, 0, 0 );					// Render as volume (in channel 0, out buffer 0)
	float rtime = gvdb.TimerStop();
	printf ( "Render volume. %6.3f ms\n", rtime );

	printf ( "Writing out_rendfile.png\n" );
	buf = (unsigned char*) malloc ( w*h*4 );
	gvdb.ReadRenderBuf ( 0, buf );
}

void VoxelRender::engine(int y, int x, int r, ChannelMask channels, Row& row)
{
    // fill buffer with grey color
    foreach(z, channels)
    {
        float* outptr = row.writable(z);
        for (int cur = x ; cur < r ; cur++)
        {
            outptr[cur] = 0.18f;
        }
    }
}

static Iop* build(::Node* node)
{
    return new VoxelRender(node);
}

const Iop::Description VoxelRender::description(CLASS, "VoxelRender", build);